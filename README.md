# Moved to github  -> github.com/ndyakov/dotfiles #
# dotfiles

My dotfiles are separated in few branches:

 * kiba - Those that I use for my laptop.
 * moon - The ones that are on my router.
 * vim  - As it is anywhere - a separate branch for vim configurations.
 * irssi - My irssi setup with some plugins and aliases ( needs few custom bash
   scripts to run some commands like play and stop ) 
###You can clone a single branch with something like this:
```shell
git clone -b vim --single-branch git@bitbucket.org:ndyakov/dotfiles.git vim
```